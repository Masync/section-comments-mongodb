// libreria para datos sensible
require('dotenv').config({path: 'src/.env'})
// orm de mongoDb
const mongoose = require('mongoose');

// conexion a mongoDB
mongoose.connect(process.env.DBCONECT,
    {
        useNewUrlParser: true, useUnifiedTopology: true
       })
       .then(() => {
           console.log(`DB ok`);
       });