// controladores de los endpoints
const {index,commentsPost,commentPut,commentDelete,notfound,controllerCore} = require('../controllers/controller.index')
// Modulo de rutas de express
const {Router} = require('express');
const routes = Router();

// index
routes.get('/api/comments', index);
// post comentarios
routes.post('/api/comments', commentsPost);
// put comentarios
routes.put('/api/comments:id', commentPut)
// deleted comentario
routes.delete('/api/comments/:id', commentDelete)

routes.get('/core', controllerCore)
// 404 not found
// routes.get('*', notfound);


// exportanto el modulo routes
module.exports = routes;