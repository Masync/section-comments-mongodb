
// librerias
const morgan = require('morgan');
const path = require('path');
require('./bd')
require('dotenv').config({path: 'src/.env'})
const routes = require('./routes/routes.index')
const express = require('express');
const app = express();


// conocer las peticiones del servidor
app.use(morgan('dev'));
// admiten json
app.use(express.json());
// admite respuesta
app.use(express.urlencoded({extended: false}));
// puerto
app.set('port', process.env.PORT);
// rutas
app.use(routes);
// archivos publicos para el servidor
app.use(express.static(`${__dirname}/public`));
//
app.set('view engine', 'ejs');
//
app.set('views', path.join(__dirname,'views'));


// Servidor
app.listen(app.get('port'), ()=>{
    console.log(`puerto ${app.get('port')}`);
})
