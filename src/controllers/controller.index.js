// libs de moment.js y del modelo de mongoDB
const commentS = require('../models/model.comment')

// endpoint get para la visualizacion de los comentarios 
const index = async(req, res, next)=>{
    // buscando en la base de datos los comentarios
    await commentS.find({}, (err, data)=>{

        //  validacion de respuestas con mongo y el servidor
         if(err) res.status(500).send({message: 'error con la peticion'});
         if(!data) res.status(404).send({message: 'no se encontro elemento'});
        res.status(200).send({data});
    });
}
// endpoint post para la creacion del comentario
const commentsPost = async(req, res, next) =>{
    // instanciando el schema de la bd para crear un mensaje
    const comment = new commentS();
    // datos de la peticion del usuario
    const {correo,titulo,descripcion} = req.body;

    comment.email = correo;
    comment.title = titulo;
    comment.desc = descripcion;
    // Guardando el comentario
   await comment.save((err, pStorage)=>{
        if (err) res.status(500).send({message:`error al guardar`});
        res.status(200).send({comment: pStorage});
    }); 
}

// endpoint para actualizar
const commentPut = async (req, res)=>{
    await commentS.findByIdAndUpdate(req.params.id, req.body)
    
    res.json({
        status: 'comment update'
    });
}
// endpoint para eliminar
const commentDelete = async (req, res) =>{
    await commentS.findByIdAndRemove(req.params.id);
    res.json({
        status: 'comment deleted'
    })
}
// manejo de errores 404 not found
const notfound = (req, res, next)=>{
    res.status('404').json({"status":"404 not found"})
}

const controllerCore = (req, res) =>{
    res.render('../views/core.ejs')
}

// exportando los controladores
module.exports = {
    index,
    commentsPost,
    commentPut,
    commentDelete,
    notfound,
    controllerCore
}