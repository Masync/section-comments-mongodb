const {VueLoaderPlugin} = require('vue-loader')

module.exports = {
    entry: './src/app/index.js',
    output: {
       path: __dirname + '/src/public/js',
       filename: 'bundle.js'
    },
    
    module: {
        rules: [
            { test: /\.vue$/, loader: 'vue-loader', exclude: /node_modules/ },
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
            { test: /\.css$/, loader: ['style-loader', 'css-loader'] }
          ]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};